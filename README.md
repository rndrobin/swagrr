# SwagRR #

This is a convenience wrapper for swagger.io, it contains "swagger-jsdoc" and "swagger-tools".

[![npm Version](https://img.shields.io/npm/v/swagrr.svg)](https://www.npmjs.com/package/swagrr)
[![npm Downloads](https://img.shields.io/npm/dm/swagrr.svg)](https://www.npmjs.com/package/swagrr)
[![Known Vulnerabilities](https://snyk.io/test/npm/swagrr/badge.svg)](https://snyk.io/test/npm/swagrr)

### Reference Links: ###

* Swagger.io Documentation:     http://swagger.io/specification
* Swagger JsDoc:                https://www.npmjs.com/package/swagger-jsdoc
* Swagger Test:                 https://www.npmjs.com/package/swagger-test
* Tutorial:                     http://mherman.org/blog/2016/05/26/swagger-and-nodejs/#.WFgW2xt96Cr

## Supported Swagger Versions
* 2.0

## Install

```bash
$ npm install swagrr --save
```

### Quick Start

```js
const swagrr = require('swagrr');
const app = express();
const bodyParser = require('body-parser');
const http = require('http');

app.use(cookieParser());
app.use(bodyParser.json({limit: '4mb'}));
app.use(bodyParser.urlencoded({extended: true}));

// Wrap your app initialization with swagger initialization so it can record your routes
swagrr.initAppWithSwagger(app, {
    title: 'Hello World API via Swagger',
    description: 'HelloWorld (semi)auto-generated api documentation!',
    version: '0.1',
    host: 'localhost:7777',
    isUseToken: true,
    isForceForProduction: true, // Omit this or use false to skip swagger loading in production environment
    apiPaths: './routes/*.js', './app/models/*.js',
    customSwaggerUiDir: './swagger-ui-custom' // Omit to use default swagger-ui html
}, function (app) {
    // Init your routes here
    app.use(require('../routes'));

    // Start the server
    const server = http.createServer(app).listen(app.get('port'), function () {
        console.log('Listening on http://localhost:' + app.get('port'));
    });

    process.on('SIGTERM', function () {
        server.close(function () {
            process.exit(0);
        });
    });
});
```

### Need help? ###

[Lesh_M @ Round Robin](mailto:leshem@rndrobin.com?subject=SwagRR)