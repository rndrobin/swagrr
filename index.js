/**
 * @param {express} app The express app
 * @param {object} params
 * @param {string} [params.title] The app title
 * @param {string} [params.description] The app description
 * @param {string} [params.version] The app version e.g. '1.0'
 * @param {string} params.host The host (+port) e.g. 'localhost:7777'
 * @param {string} [params.basePath] The base path e.g. '/'
 * @param {boolean} [params.isUseToken] Use token authentication
 * @param {boolean} [params.isUseMockData] Use mock data when testing APIs
 * @param {boolean} [params.isForceForProduction] Always init swagger, even for production environments
 * @param {boolean} [params.isOnlyUI] Prevent swagger from performing any validations on requests and responses
 * @param {string} [params.controllersPath] The path to the controllers folder
 * @param {Array} params.apiPaths An array with paths to api files (with wildcards) e.g. ['./routes/*.js', './app/models/*.js']
 * @param {string} [params.customSwaggerUiDir] The path to a custom swagger-ui folder
 * @param {Function} callback
 */
exports.initAppWithSwagger = function (app, params, callback) {
    if (params.isForceForProduction || process.env.NODE_ENV != 'production') {
        const swagger = require('swagger-tools');
        const swaggerJSDoc = require('swagger-jsdoc');

        // swagger definition
        const swaggerDefinition = {
            // swaggerDocumentation:    'http://swagger.io/specification/',
            // swaggerJssDoc:           'https://github.com/Surnet/swagger-jsdoc',
            // swagger-test:            'https://www.npmjs.com/package/swagger-test',
            // tutorial:                'http://mherman.org/blog/2016/05/26/swagger-and-nodejs/#.WFgW2xt96Cr',
            info: {
                title: params.title || process.env.SWAG_TITLE || 'Round Robin App',
                description: params.description || process.env.SWAG_DESC || 'App description',
                version: params.version || process.env.SWAG_VERSION || '1.0'
            },
            host: params.host || process.env.SWAG_HOST || 'localhost',
            basePath: params.basePath || process.env.SWAG_BASE_PATH || '/'
        };

        if (params.isUseToken || process.env.SWAG_USER_TOKEN) {
            swaggerDefinition.securityDefinitions = swaggerDefinition.securityDefinitions || {};

            swaggerDefinition.securityDefinitions.token = {
                type: 'apiKey',
                name: 'Authorization',
                in: 'header'
            }
        }

        // options for the swagger docs
        var options = {
            // import swaggerDefinitions
            swaggerDefinition: swaggerDefinition,
            // path to the API docs
            apis: params.apiPaths
        };

        // initialize swagger-jsdoc
        var swaggerSpec = swaggerJSDoc(options);

        // Initialize the Swagger Middleware
        swagger.initializeMiddleware(swaggerSpec, function (middleware) {
            if (!params.isOnlyUI) {
                // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
                app.use(middleware.swaggerMetadata());
    
                // Provide the security handlers
                // app.use(middleware.swaggerSecurity({
                //     token: function (req, def, scopes, callback) {
                //         console.log('TOKEN: ' + def);
                //     }
                // }));
    
                // Validate Swagger requests
                app.use(middleware.swaggerValidator(
                    {validateResponse: false}
                ));
    
                if (params.isUseMockData || process.env.SWAG_USE_MOCK_DATA) {
                    // Route validated requests to appropriate controller
                    app.use(middleware.swaggerRouter({
                        useStubs: process.env.NODE_ENV != 'production',
                        controllers: params.controllersPath
                    }));
                }
            }

            if (params.customSwaggerUiDir) {
                // Serve the Swagger documents and Swagger UI
                //   http://localhost:3040/docs => Swagger UI
                //   http://localhost:3040/api-docs => Swagger document
                app.use(middleware.swaggerUi({
                    swaggerUiDir: params.customSwaggerUiDir
                }));
            }

            // serve swagger
            app.get('/swagger.json', function (req, res) {
                res.setHeader('Content-Type', 'application/json');
                res.send(swaggerSpec);
            });

            callback(app);
        });
    } else {
        callback(app);
    }
};